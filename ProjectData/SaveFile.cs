﻿using Microsoft.Win32;
using System.Collections.Generic;
using System.IO;


namespace ProjectData
{
    class SaveFile
    {
        private string CurrentFile { get; set; }
      

        public SaveFile()
        {

        }

        public SaveFile(string currentFile)
        {
            this.CurrentFile = currentFile;
        }


        public string GetPath()
        {
            if (string.IsNullOrEmpty(CurrentFile))
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = "CSV Files(*.csv)|*.csv|All(*.*)|*";
                dialog.RestoreDirectory = true;
                dialog.InitialDirectory = dialog.FileName;
                if (dialog.ShowDialog() == true)
                    return dialog.FileName; 
                return null;
            }

            return CurrentFile;
        }

        public void OpenFileDialog(List<Students> a, SaveFileDialog d)
        {

            if (d.ShowDialog() == true)
            {
                
                string path = d.FileName;
                StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.GetEncoding(1251));
                using (sw)
                {
                    foreach (var item in a)
                    {
                        sw.Write(item.Name + ",");
                        sw.Write(item.SecondName + ",");
                        sw.Write(item.DepartmentId + ",");
                        sw.WriteLine();
                    }

                    d.RestoreDirectory = true;
                    sw.Close();
                }

            }
        }
    }
}
