﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectData
{
    public class Student
    {
        public string Name { get; set; }
        public string LastName { get; set; }

        public Student(string name, string lastName)
        {
            Name = name;
            LastName = lastName;
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", Name, LastName);
        }
    }
}
