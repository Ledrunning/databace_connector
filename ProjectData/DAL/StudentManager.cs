﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;
using System.Configuration;

// Убери Департмент ID!!!! Ключ удален!!!
// Чтобы не выделялся DataGrid в свойствах пометил галочкой ReadOnly;
namespace ProjectData
{
    static class StudentManager
    {

      public static event EventHandler Changed; // Событие со встроенным делегатом;
      public static event EventHandler DeleteAll; 
      // Адрес установил в AppConfig;
      static string DbAddress = ConfigurationManager.ConnectionStrings["StudentConnection"].ConnectionString;
      
       static StudentManager() { }

            public static List<Students> ReadData()
            {
                using (SqlConnection con = new SqlConnection(DbAddress))
                {
                    string select = "SELECT * FROM Student";
                    SqlCommand cmd = new SqlCommand(select, con);
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    List<Students> tableList = new List<Students>();

                    while (reader.Read())
                    {
                        tableList.Add(new Students(Convert.ToInt32(reader["Id"]), reader["Name"].ToString(), 
                                              reader["SecondName"].ToString(), Convert.ToInt32(reader["DepartmentID"])));
                        
                    }

                con.Close();
                    return tableList;
                }
                   
                            
            }

            public static bool IsFull()
            {
                using (SqlConnection con = new SqlConnection(DbAddress))
                {
                    string selectRequest = "SELECT * FROM Student";
                    SqlCommand cmd = new SqlCommand(selectRequest, con);
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    List<Students> tableList = new List<Students>();
                    while (reader.Read())
                    {
                         tableList.Add(new Students (Convert.ToInt32(reader[0]), reader[1].ToString(), 
                                                    reader[2].ToString(), Convert.ToInt32(reader[3])));
                    }

                if (tableList != null && tableList.Count > 0)
                    return true;
                return false;
                }
            }
                public static void InsertData(Students s)
                {

                    using (SqlConnection con = new SqlConnection(DbAddress))
                    {
                       // string insert = String.Format("INSERT INTO Student (Name, SecondName, DepartmentId) VALUES (N'{0}', N'{1}', N'{2}')", n, sn, di);
                        string insert = "INSERT INTO Student (Name, SecondName, DepartmentId) VALUES (@Name, @SecondName, @DepartmentId)";
                        SqlCommand cmd = new SqlCommand(insert, con);
                        // Защищаем от ввода SQL комманд;
                        SqlParameter parametrName = new SqlParameter("Name", s.Name);
                        SqlParameter parametrSecondName = new SqlParameter("SecondName", s.SecondName);
                        SqlParameter parametrDepartmentId = new SqlParameter("DepartmentId", s.DepartmentId);

                        cmd.Parameters.Add(parametrName);
                        cmd.Parameters.Add(parametrSecondName);
                        cmd.Parameters.Add(parametrDepartmentId);

                        try
                        {
                            con.Open();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message);
                        }
                        finally
                        {
                            con.Close();
                        }

                       if(Changed != null)  // В новой студии можно просто Changed??.Invoke;
                       {
                           Changed.Invoke(null, new EventArgs());
                       }
                   }
            
               }



                public static void DeleteFromDB(int id)
                {
                    using (SqlConnection con = new SqlConnection(DbAddress))
                    {
                           string del = String.Format("DELETE FROM Student WHERE Id = {0}", id);
                           SqlCommand cmd = new SqlCommand(del, con);
                           con.Open();
                           //var tr = con.BeginTransaction();
                           //tr.Commit();
                           cmd.ExecuteNonQuery();
                    }

                    if (Changed != null)  // В новой студии можно просто Changed??.Invoke;
                    {
                        Changed.Invoke(null, new EventArgs());
                    }

                }

                public static void DeleteAllDB()
                {
                    using (SqlConnection con = new SqlConnection(DbAddress))
                    {
                        string delAll = "DELETE FROM Student";
                        SqlCommand cmd = new SqlCommand(delAll, con);
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }

                        if (DeleteAll != null)
                        {
                            DeleteAll.Invoke(null, new EventArgs());
                        }

                }

                public static void EditDB(Students s)
                {
                  using (SqlConnection con = new SqlConnection(DbAddress))
                  {
                      con.Close();
                      string upd = "UPDATE Student SET Name = @Name, SecondName = @SecondName WHERE id = @id";
                      SqlCommand cmd = new SqlCommand(upd, con);
                      SqlParameter parametrName = new SqlParameter("Name", s.Name);
                      SqlParameter parametrSecondName = new SqlParameter("SecondName", s.SecondName);
                      SqlParameter parametrId = new SqlParameter("id", s.id);

                      cmd.Parameters.Add(parametrName);
                      cmd.Parameters.Add(parametrSecondName);
                      cmd.Parameters.Add(parametrId);

                      con.Open();
                      cmd.ExecuteNonQuery();
                  }
                  
              }

            public static void FillDB(string[] sfn, string[] sln)
            {
                Random rnd = new Random();

                for (int i = 0; i < sfn.Length; i++)
                {
                   InsertData(new Students(sfn[i], sln[i], rnd.Next(0,8)));
                }
            }

            
            

    }

 }
