﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectData
{
public class Students
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public int DepartmentId { get; set; }

        public Students() { }
  
        public Students(int id, string n, string sn, int dId)
         {
                this.id = id;
                Name = n;
                SecondName = sn;
                DepartmentId = dId;
         }

        public Students(string n, string sn, int dId)
        {
            Name = n;
            SecondName = sn;
            DepartmentId = dId;
        }
    
    }
}
