﻿using System;
using System.Windows;
 
namespace ProjectData
{
    /// <summary>
    /// Логика взаимодействия для EditForm.xaml
    /// </summary>
    public partial class EditForm : Window
    {
        Students currentStudent = new Students();

        public EditForm()
        {
            InitializeComponent();
        }

        public EditForm (Students s)
        {
            InitializeComponent();
            currentStudent = s;
        }

        private void OkButton(object sender, RoutedEventArgs e)
        {
            Students s = new Students();

            if (currentStudent != null)
            {
                s = currentStudent;
            }
         
          
            if (firstName.Text != "" && secondName.Text != "")
            {
                s.Name = firstName.Text;
                s.SecondName = secondName.Text;
                int dpId;
              
                bool result = Int32.TryParse(textId.Text, out dpId);
                if (result)
                {
                    s.DepartmentId = dpId;
                }
                else
                {
                    MessageBox.Show("Введите число для Department Id!");
                }
               
              
                if (currentStudent == null)
                {
                    StudentManager.InsertData(s);
                }
             
                this.Close();
            }
            else MessageBox.Show("Заполните поля или нажмите \"Отмена\"");
        }

        private void CancelButton(object sender, RoutedEventArgs e)
        {
           this.Close();
        }

       
    }
}
