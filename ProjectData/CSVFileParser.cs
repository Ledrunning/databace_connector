﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectData
{
    class CSVFileParser
    {
        //public string NameFromFile { get; set; }
        //public string LastNameFromFile { get; set; }
        private string[] allNamesFromFile;
        private string[] studentsName;
        private string[] studentsLastName;
        List<Student> lst = new List<Student>();


        public CSVFileParser(string[] unFromFileStr)
        {
            allNamesFromFile = unFromFileStr;
        }
        
        public void ReadAndParseFile()
        {
           
            for (int i = 0; i < allNamesFromFile.Length; i++)
            {
                if (i % 2 == 0 || i == 0)
                    studentsName[i] = allNamesFromFile[i];
                else
                    studentsLastName[i] = allNamesFromFile[i];
            }
        }

        public void ListOfStudents(string[] fn, string[] ln)
        {
            for (int i = 0; i < fn.Length; i++)
            {
                Student st = new Student(fn[i], ln[i]);
                lst.Add(st);
            }
        }

        public IEnumerable<Student> GetAllStudents()
        {
            foreach (var item in lst)
            {
                yield return item;
            }
        }
    }
}
