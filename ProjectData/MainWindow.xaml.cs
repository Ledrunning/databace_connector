﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using Microsoft.Win32; // Это для OpenFileDialog; 
using System.Diagnostics;
using System.Windows.Navigation;

namespace ProjectData
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static private string sfn = "Abraham, Addison, Adrian, Albert, Alec, Alfred, Alvin, Andrew, Andy," +
                                    " Archibald, Archie, Arlo, Arthur, Arthur, Austen, Barnabe, Bartholomew, " +
                                    "Bertram, Bramwell, Byam, Cardew, Chad, Chance, Colin, Coloman, Curtis, Cuthbert, " +
                                    "Daniel, Darryl, David";

        static private string sln = "Smith, Johnson, Williams, Jones, Brown, Davis, Miller, Wilson, Moore, Taylor," +
                                    " Anderson, Thomas, Jackson, White, Harris, Martin, Thompson, Wood, Lewis, Scott, " +
                                    "Cooper, King, Green, Walker, Edwards, Turner, Morgan, Baker, Hill, Phillips";

        public static string[] usersFirstNames = sfn.Split(new string[] { ", ", "\n", "\t" }, StringSplitOptions.RemoveEmptyEntries);
        public static string[] usersLastNames = sln.Split(new string[] { ", ", "\n", "\t" }, StringSplitOptions.RemoveEmptyEntries);

        string currentFile, currentSaveAsfile;

        public MainWindow()
        {
            InitializeComponent();
            StudentManager.Changed += StudentManager_Changed;
        }

        private void StudentManager_Changed(object sender, EventArgs e)
        {
            try
            {
                ListOfStudents.ItemsSource = StudentManager.ReadData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!",
                                MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                ListOfStudents.ItemsSource = StudentManager.ReadData();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!",
                               MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // Добавить в БД;
        private void AddButton(object sender, RoutedEventArgs e)
        {
             
                EditForm ef = new EditForm(null);
                ef.Owner = this;
                ef.ShowDialog();
               
        }

        // Удаление из БД;
        private void DeleteButton(object sender, RoutedEventArgs e)
        {
            try
            {
                int selectedId = ((Students)ListOfStudents.SelectedItem).id;
                StudentManager.DeleteFromDB(selectedId);
            } 
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!",
                              MessageBoxButton.OK, MessageBoxImage.Error);
            }
 
        }

        /// <summary>
        /// Данное событие предназначено для вызова окна EditForm
        /// в котором происходит редактирование данных
        /// Данный код позволяет извлекать содержимое каждой ячейки
        /// для передачи в окно EditForm
        /// </summary>
       
        private void EditButton(object sender, RoutedEventArgs e)
        {
            //EditForm ef = new EditForm(null);
            //ef.Owner = this;
            //ef.ShowDialog();
            //Students selectedId = ((Students)ListOfStudents.SelectedItem);
            //StudentManager.EditDB(selectedId);
           
            if (selectTrue)
            {
                EditForm editData = new EditForm();
                
                var selectedCell = ListOfStudents.SelectedCells[selectedColumn];
                var cellContent = selectedCell.Column.GetCellContent(selectedCell.Item);

                for (int i = 1; i < 4 ; i++)
                {
                    selectedCell = ListOfStudents.SelectedCells[i];
                    cellContent = selectedCell.Column.GetCellContent(selectedCell.Item);

                    switch (i)
                    {
                        case 1:
                            editData.firstName.Text = (cellContent as TextBlock).Text;
                            break;

                        case 2:
                            editData.secondName.Text = (cellContent as TextBlock).Text;
                            break;

                        case 3:
                            editData.textId.Text = (cellContent as TextBlock).Text;
                            break;
                    }

                }
                editData.Show();
            }
            else
                MessageBox.Show("No entry selected!");
        }

        // Этот костыль не работает;
        //private void grid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        //{
        //    Students buff = (Students)e.Row.Item;
        //    StudentManager.EditDB(buff);
        //}

        // Обновить;
        private void RefreshButton(object sender, RoutedEventArgs e)
        {
            try
            {
                ListOfStudents.ItemsSource = StudentManager.ReadData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!",
                             MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // Заполнить БД из массива.;
        private void FillFromArraysButton(object sender, RoutedEventArgs e)
        {
            try
            {
                StudentManager.FillDB(usersFirstNames, usersLastNames);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!",
                           MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // Удалить все;
        private void DeleteAllFromDBButton(object sender, RoutedEventArgs e)
        {
            const string message =
            "Are you sure to delete all data?";
            const string caption = "Attention!";
           
            if (StudentManager.IsFull())
            {
                MessageBoxResult result = MessageBox.Show(message, caption,
                                          MessageBoxButton.OKCancel,
                                          MessageBoxImage.Question);
                // Проверка, нажата - ли кнопка;
                if (result == MessageBoxResult.OK)
                    StudentManager.DeleteAllDB();
                ListOfStudents.ItemsSource = StudentManager.ReadData();
            }
            else MessageBox.Show("Data base is empty...", caption,
                                MessageBoxButton.OK, MessageBoxImage.Information);
        }

        // Заполнить БД из файла;
        private void ExportFileButton(object sender, RoutedEventArgs e)
        {
          //  StudentManager.FillDB(namesFromFile, )
        }

        
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        // Open file;
        private void openClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            Stream fileStream = null;
            openFile.InitialDirectory = "c:\\";
            openFile.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            openFile.FilterIndex = 2;
            openFile.RestoreDirectory = true;

            if (openFile.ShowDialog() == true)
            {
                string studentsNameFromFile;
                string[] parsedStudentsDataFromFile;

                try
                {
                    if ((fileStream = openFile.OpenFile()) != null)
                    {
                        using (fileStream)
                        {
                            // Insert code to read the stream here.
                            StreamReader sr = new
                            StreamReader(openFile.FileName);
                            studentsNameFromFile = (sr.ReadToEnd());
                            parsedStudentsDataFromFile = studentsNameFromFile.Split(new string[] { ", ", "\n", "\t" }, StringSplitOptions.RemoveEmptyEntries);
                            // Нужно из файла выдернуть отдельно имя, отдельно Фамилию.
                            CSVFileParser fileParser = new CSVFileParser(parsedStudentsDataFromFile);
                            sr.Close();

                        }
                    }

                   

                }

                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }

            }
        }

        // Сохранаяем в файл из БД;
        private void saveClick(object sender, RoutedEventArgs e)
        {
            const string caption = "Error!";
            SaveFile sf = new SaveFile(currentFile);
            var rdDataFromDB = StudentManager.ReadData();
            
            try
            {
                string path = sf.GetPath();
                if (string.IsNullOrEmpty(path)) return;

                currentFile = path;
                StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.GetEncoding(1251));
                using (sw)
                {
                    foreach (var item in rdDataFromDB)
                    {
                        sw.Write(item.Name + ",");
                        sw.Write(item.SecondName + ",");
                        sw.Write(item.DepartmentId + ",");
                        sw.WriteLine();
                    }

                }
            }
          
        
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, caption, MessageBoxButton.OK, MessageBoxImage.Error);
            }
}

        // Save file as...
        private void saveAsclick(object sender, RoutedEventArgs e)
        {
            const string caption = "Error!";
            const string message = "Error while saving!";
            var rdDataFromDB = StudentManager.ReadData();
            SaveFile sf = new SaveFile(currentSaveAsfile);
            SaveFileDialog safeFile = new SaveFileDialog();
            safeFile.Filter = "CSV Files(*.csv)|*.csv|All(*.*)|*";
            safeFile.RestoreDirectory = true;
            safeFile.InitialDirectory = "c:\\";

            try
            {
                sf.OpenFileDialog(rdDataFromDB, safeFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show(message + ex.Message, caption, MessageBoxButton.OK, MessageBoxImage.Error);
            }
           
 
            
        }

        // Close programm;
        private void exitClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
        // Help open;
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start("help.pdf");
            }
            catch (IOException ex)
            {
                MessageBox.Show("File doesn't exist. " + ex.Message, "Error!",
                                 MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch(Exception ex)
            {
                MessageBox.Show("System Error. " + ex.Message, "Error!",
                               MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AboutClick(object sender, RoutedEventArgs e)
        {
            About ab = new About();
            ab.Owner = this;
            ab.ShowDialog();
        }

        // Обработчик DataGrid;

        int selectedColumn = 0;
        bool selectTrue = false;

        private void ListOfStudents_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedColumn = ListOfStudents.CurrentCell.Column.DisplayIndex;
            selectTrue = true;
        }

        private void students_loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                ListOfStudents ls = new ListOfStudents(MainWindow.usersFirstNames, MainWindow.usersLastNames);
                grid.ItemsSource = ls.GetAllStudents();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!",
                               MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

       
    }
}


