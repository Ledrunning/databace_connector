﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectData
{
    class ListOfStudents
    {
        List<Student> lst = new List<Student>();

        public ListOfStudents(string[] fn, string[] ln)
        {
            for (int i = 0; i < fn.Length; i++)
            {
                Student st = new Student(fn[i], ln[i]);
                lst.Add(st);
            }
        }

        public IEnumerable<Student> GetAllStudents()
        {
            foreach (var item in lst)
            {
                yield return item;
            }
        }
    }
}
